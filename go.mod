module gitlab.com/oolongbrothers/unflac

go 1.13

require (
	git.sr.ht/~ft/cue v0.0.0-20200508193024-48a5e773bbae
	github.com/asdfsx/gochardet v0.0.0-20170222172924-16496b196583
	github.com/gammazero/workerpool v0.0.0-20191211212902-79f70a4b5331
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	golang.org/x/text v0.3.2
)
